#![allow(non_camel_case_types)]
#![feature(try_from)]
#[macro_use] extern crate finite_fields;

/// Creates a unit ternary type named `t1`.
unit_nary! { t1, 3, u8 }

fn main() {
  println!("{}", (0..2).fold(ZERO, |accum, _| (accum + ONE).unwrap()));
}
