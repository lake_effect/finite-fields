#![allow(non_camel_case_types)]
#![feature(try_from)]
#[macro_use] extern crate finite_fields;

binary_type! { b2, 2 }

fn main() {
  let foo: b2 = b2::new([ZERO, ONE]);
  let bar = foo >> 1;

  for val in foo.iter().rev() {
    println!("{:?}", val);
  }

  let baz = vec![foo.clone(), bar.clone(), foo.clone()];
  let bat = vec![foo];

  let quux = bat.iter().chain(baz.iter());
  print!("foo bits: ");
  for bit in foo.bits().iter() {
    print!("{}, ", bit);
  }

  print!("foo: {}", foo);
}
