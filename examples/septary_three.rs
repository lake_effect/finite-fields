#![allow(non_camel_case_types)]
#![feature(try_from)]
#[macro_use] extern crate finite_fields;

/// Creates a septary type named `s3`, with a unit type named `s1`, each digit
/// stored in a `u8`, with three digits.
nary_type! { s3, s1, 7, u8, 3 }

fn main() {
  let foo = s3::new([ZERO, ZERO, ONE]);

  let bar: s3 = foo.iter().map(|item| {
    (item + ONE).unwrap()
  }).collect();

  for val in bar {
    println!("{:?}", val);
  }

  let baz = vec![foo.clone(), bar.clone(), foo.clone()];
  let bat = vec![foo];

  let quux = bat.iter().chain(baz.iter());

  print!("foo: {}", foo);
}
