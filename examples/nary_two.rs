#![allow(non_camel_case_types)]
#![feature(try_from)]
#[macro_use] extern crate finite_fields;

/// Creates a ternary type named `t2`, with a unit type named `t1`, storing each
/// digit in a `u8`, with two digits.
nary_type! { t2, t1, 3, u8, 2 }

fn main() {}
