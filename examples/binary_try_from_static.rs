#![feature(try_from)] extern crate finite_fields;
use std::convert::TryInto;
use finite_fields::binary_static::{b2, b1, ZERO, ONE};

fn main() {
  let my_zero: b1 = (0 as usize).try_into().unwrap();

  let three: usize = b2::new([ONE, ONE]).into();
  let three_bin = b2::new([ZERO, ONE]);

  println!("three (binary): {}, three: {}", three_bin, three);
}
