#![allow(non_camel_case_types)]
#![feature(try_from)]
#[macro_use] extern crate finite_fields;

binary_type! { b2, 3 }

fn main() {

  let foo = b2::new([ZERO, ZERO, ONE]);

  // let bar: b2 = foo.iter().map(|item| {
  //   item ^ ONE
  // }).collect();

  // for val in bar {
  //   println!("{:?}", val);
  // }

  for val in foo.iter().rev() {
    println!("reverse: {:?}", val);
  }
}
