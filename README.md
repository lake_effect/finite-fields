# finite-fields

![cargo version badge](https://img.shields.io/crates/v/finite-fields.svg)

A crate providing explicit syntax for non-stdlib finite field types.

## details

### design goals

There are two parts to the crate's design:

- Semantics: The types should facilitate direct expression of numeric types
  ranging over finite fields, so that the ideas expressed in the code are clear
  on their face.

- Syntax: Syntax for working with the types should be transparent and universal
  where reasonable. For example, `Add` for `GF(2)` should present the same
  interface as `Add` for `GF(4)`.

### implementation

The implementations are not great, but they should work as a demonstration. The
crate currently uses fixed-length arrays, with the idea that types be created by
the programmer using macros. In other words, it stores the factored elements
(the 'digits') of a number in the field as elements of a finite-length array.

## roadmap

- [x] Binary types
- [x] Binary macros
- [x] Big refactor about here
- [x] Non-binary types
- [x] Non-binary macros
- [ ] Rigorous tests
- [ ] Refactor down huge macros
- [ ] Tiny pieces

## weird readings

- A
  [project to discover generalized binary number systems up to 11 dimensions](http://szdg.lpds.sztaki.hu/szdg/desc_numsys_es.php).
- [How MATLAB users think of binary numbers](https://www.mathworks.com/matlabcentral/newsreader/view_thread/48679)
