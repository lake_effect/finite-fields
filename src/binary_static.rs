use std::fmt;
use std::cmp::Ordering;
use std::convert::From;
use std::iter::{FromIterator, IntoIterator, Iterator};
use std::ops::{Add,
               Sub,
               Mul,
               Div,
               BitAnd,
               BitOr,
               BitXor,
               Index,
               IndexMut,
               Shl,
               Shr};
use error::{DivisionError, OverflowError};

#[cfg(feature = "nightly")]
use std::convert::TryFrom;
#[cfg(feature = "nightly")]
use error::ConversionError;

/// The base binary type, a unit-width binary digit.
#[derive(Clone,Copy,PartialEq,Eq,Debug)]
pub struct b1(bool);

NewtypeFrom! {
  () pub struct b1(bool);
}

impl b1 {
  pub fn new(val: bool) -> b1 {
    b1(val)
  }

  /// Reports the maximum unsigned integer expressible in this type.
  pub fn max(&self) -> usize {
    1
  }
}

impl fmt::Display for b1 {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self.0 {
      false => write!(f, "{}", 0),
      true => write!(f, "{}", 1)
    }
  }
}

/// Shorthand for zero in unit-width binary type.
pub const ZERO: b1 = b1(false);
/// Shorthand for one in unit-width binary type.
pub const ONE: b1 = b1(true);

impl<'a> PartialEq<b1> for &'a b1 {
  fn eq(&self, other: &b1) -> bool {
    self.0 == other.0
  }
}
impl<'a> PartialEq<&'a b1> for b1 {
  fn eq(&self, other: &&'a b1) -> bool {
    self.0 == other.0
  }
}

#[cfg(feature = "nightly")]
/// Fallible conversion by simple exhaustive matching.
///
/// # Examples
///
/// ```
/// #![feature(try_from)] extern crate finite_fields;
/// use std::convert::TryInto;
/// use finite_fields::binary_static::b1;
///
/// let zero: b1 = (0 as usize).try_into().unwrap();
/// assert_eq!(zero, b1::new(false));
///
/// let one: b1 = (1 as usize).try_into().unwrap();
/// assert_eq!(one, b1::new(true));
/// ```
impl TryFrom<usize> for b1 {
  type Err = ConversionError;

  fn try_from(val: usize) -> Result<Self, Self::Err> {
    match val {
      0 => Ok(ZERO),
      1 => Ok(ONE),
      _ => Err(ConversionError::OverflowError { arg1: val.to_string() })
    }
  }
}

impl From<b1> for usize {
  fn from(val: b1) -> usize {
    match val {
      ZERO => 0,
      ONE => 1
    }
  }
}

/// Peano arithmetic operators.
pub trait Peano where Self: Sized {
  /// Produces the next integer value in the field of self (i.e., increment).
  fn successor(&self) -> Result<Self, OverflowError>;
  /// Produces the previous integer value in the field of self (i.e.,
  /// decrement).
  fn predecessor(&self) -> Result<Self, OverflowError>;
  /// Total ordering function.
  fn cmp (&self, other: &Self) -> Ordering;
}

impl Peano for b1 {
  fn successor(&self) -> Result<b1, OverflowError> {
    match self {
      &ZERO => Ok(ONE),
      &ONE => Err(OverflowError::Default { arg1: self.to_string(),
                                           arg2: ONE.to_string() })
    }
  }

  /// Peano arithmetic function.
  fn predecessor(&self) -> Result<b1, OverflowError> {
    match self {
      &ONE => Ok(ZERO),
      &ZERO => Err(OverflowError::Default { arg1: self.to_string(),
                                            arg2: ONE.to_string() })
    }
  }

  /// Total ordering function.
  fn cmp (&self, other: &Self) -> Ordering {
    if &self == other { Ordering::Equal }
    else if let &Ok(next) = &self.successor() {
      if next == other { Ordering::Less }
      else { Ordering::Greater }
    }
    else { Ordering::Greater }
  }
}

impl PartialOrd for b1 {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

/// Arithmetic addition with overflow error.
impl Add for b1 {
  type Output = Result<b1, OverflowError>;
  fn add(self, other: b1) -> Result<b1, OverflowError> {
    match other {
      ZERO => Ok(self),
      ONE => self.successor()
    }
  }
}
impl<'a> Add<b1> for &'a b1 {
  type Output = Result<b1, OverflowError>;
  fn add(self, other: b1) -> Result<b1, OverflowError> {
    match other {
      ZERO => Ok(*self),
      ONE => self.successor()
    }    
  }
}
impl<'a> Add<&'a b1> for &'a b1 {
  type Output = Result<b1, OverflowError>;
  fn add(self, other: &b1) -> Result<b1, OverflowError> {
    match other {
      &ZERO => Ok(ZERO),
      &ONE => self.successor()
    }
  }
}

/// Arithmetic subtraction with overflow as error.
impl Sub for b1 {
  type Output = Result<b1, OverflowError>;
  fn sub(self, other: b1) -> Result<b1, OverflowError> {
    match other {
      ZERO => Ok(self),
      ONE => self.predecessor()
    }
  }
}
impl<'a> Sub<b1> for &'a b1 {
  type Output = Result<b1, OverflowError>;
  fn sub(self, other: b1) -> Result<b1, OverflowError> {
    match other {
      ZERO => Ok(*self),
      ONE => self.predecessor()
    }
  }
}
impl<'a> Sub<&'a b1> for &'a b1 {
  type Output = Result<b1, OverflowError>;
  fn sub(self, other: &b1) -> Result<b1, OverflowError> {
    match other {
      &ZERO => Ok(*self),
      &ONE => self.predecessor()
    }
  }
}

impl Mul for b1 {
  type Output = Result<Self, OverflowError>;
  fn mul(self, other: b1) -> Result<b1, OverflowError> {
    Ok(b1(self.0 && other.0))
  }
}
impl<'a> Mul<b1> for &'a b1 {
  type Output = Result<b1, OverflowError>;
  fn mul(self, other: b1) -> Result<b1, OverflowError> {
    Ok(b1(self.0 && other.0))
  }

}
impl<'a> Mul<&'a b1> for &'a b1 {
  type Output = Result<b1, OverflowError>;
  fn mul(self, other: &b1) -> Result<b1, OverflowError> {
    Ok(b1(self.0 && other.0))
  }
}

impl Div for b1 {
  type Output = Result<Self, DivisionError>;
  fn div(self, other: b1) -> Result<b1, DivisionError> {
    if other == ZERO {
      Err(DivisionError::DivideByZeroError {
        arg1: self.to_string()
      })
    } else { Ok(self) }
  }
}
impl<'a> Div<b1> for &'a b1 {
  type Output = Result<b1, DivisionError>;
  fn div(self, other: b1) -> Result<b1, DivisionError> {
    if other == ZERO {
      Err(DivisionError::DivideByZeroError {
        arg1: self.to_string()
      })
    } else { Ok(*self) }
  }

}
impl<'a> Div<&'a b1> for &'a b1 {
  type Output = Result<b1, DivisionError>;
  fn div(self, other: &b1) -> Result<b1, DivisionError> {
    if other == ZERO {
      Err(DivisionError::DivideByZeroError {
        arg1: self.to_string()
      })
    } else { Ok(*self) }
  }
}

impl BitAnd for b1 {
  type Output = Self;
  fn bitand(self, other: b1) -> Self {
    b1(self.0 & other.0)
  }
}
impl<'a> BitAnd<b1> for &'a b1 {
  type Output = b1;
  fn bitand(self, other: b1) -> b1 {
    b1(self.0 & &other.0)
  }
}
impl<'a> BitAnd<&'a b1> for &'a b1 {
  type Output = b1;
  fn bitand(self, other: &b1) -> b1 {
    b1(self.0 & other.0)
  }
}


impl BitOr for b1 {
  type Output = Self;
  fn bitor(self, other: b1) -> Self {
    b1(self.0 | other.0)
  }
}
impl<'a> BitOr<b1> for &'a b1 {
  type Output = b1;
  fn bitor(self, other: b1) -> b1 {
    b1(self.0 | other.0)
  }
}
impl<'a> BitOr<&'a b1> for b1 {
  type Output = b1;
  fn bitor(self, other: &b1) -> b1 {
    b1(self.0 | other.0)
  }
}

impl BitXor for b1 {
  type Output = Self;
  fn bitxor(self, other: b1) -> Self {
    b1(self.0 ^ other.0)
  }
}
impl<'a> BitXor<b1> for &'a b1 {
  type Output = b1;
  fn bitxor(self, other: b1) -> b1 {
    b1(self.0 ^ other.0)
  }
}
impl<'a> BitXor<&'a b1> for &'a b1 {
  type Output = b1;
  fn bitxor(self, other: &b1) -> b1 {
    b1(self.0 ^ other.0)
  }
}

/// A two-digit binary number.
#[derive(Clone, Copy, PartialEq, Debug)]
pub struct b2([b1; 2]);

impl b2 {
  /// Array-based constructor.
  pub fn new(vals: [b1; 2]) -> b2 {
    b2(vals)
  }

  /// Reports the maximum unsigned integer expressible in this type.
  pub fn max(&self) -> usize {
    3
  }

  /// Reports the number of digits in this type.
  pub fn len(&self) -> usize {
    2
  }

  pub fn iter(self) -> Iter {
    Iter { data: self, index: 0, end: self.len() }
  }

  /// Copies out the internal data as a fixed-width array.
  pub fn bits(&self) -> [b1; 2] {
    self.0
  }

  /// Shifts the leftmost digit off and pushes a new digit onto the right.
  pub fn shift_concat(&self, val: b1) -> b2 {
    let mut shifted_bin = *self << 1;
    shifted_bin[self.len() - 1] = val;
    shifted_bin
  }

  /// Pops the rightmost digit off and unshifts a new digit onto the left.
  pub fn unshift_concat(&self, val: b1) -> b2 {
    let mut unshifted_bin = *self >> 1;
    unshifted_bin[0] = val;
    unshifted_bin
  }
}

impl fmt::Display for b2 {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let mut bit_string = String::with_capacity(self.len());
    for bit in self.bits().iter() { bit_string.push_str(&bit.to_string()); }
    write!(f, "{}", bit_string)
  }
}

#[cfg(feature = "nightly")]
/// Fallible conversion by successive division.
///
/// # Examples
///
/// ```
/// #![feature(try_from)] extern crate finite_fields;
/// use std::convert::{TryFrom, TryInto};
/// use finite_fields::binary_static::{b2, ZERO, ONE};
///
/// let zero: b2 = (0 as usize).try_into().unwrap();
/// assert_eq!(zero, b2::new([ZERO, ZERO]));
///
/// let one: b2 = (1 as usize).try_into().unwrap();
/// assert_eq!(one, b2::new([ZERO, ONE]));
///
/// let two: b2 = (2 as usize).try_into().unwrap();
/// assert_eq!(two, b2::new([ONE, ZERO]));
///
/// let three: b2 = (3 as usize).try_into().unwrap();
/// assert_eq!(three, b2::new([ONE, ONE]));
/// 
/// let four = b2::try_from(4 as usize);
/// assert!(four.is_err());
/// ```
impl TryFrom<usize> for b2 {
  type Err = ConversionError;

  fn try_from(val: usize) -> Result<Self, Self::Err> {
    let mut out_val = b2::new([ZERO, ZERO]);

    if val > out_val.max() {
      Err(ConversionError::OverflowError { arg1: val.to_string() })
    } else if val == 0 {
      Ok(out_val)
    } else {
      let mut dec = val.clone();
      let out_val_len = out_val.len();
      if dec % 2 != 0 {
        out_val[out_val_len - 1] = ONE;
      }
      
      for place in 0..out_val.len() {
        if dec > 1 {
          dec = dec / 2;
          out_val[place] = ONE;
        } else {
          break;          
        }
        // TODO figure out if this is ever necessary
        //
        // return Err(
        //   ConversionError::PrecisionError {
        //     arg1: val.to_string(),
        //     tgt_width: out_val_len.to_string()
        //   })
      }
      
      Ok(out_val)
    }
  }
}

/// Fallible conversion by successive division, but pretends like it's not since
/// TryFrom is still unstable.
///
/// # Examples
///
/// ```
/// extern crate finite_fields;
/// use finite_fields::binary_static::{b2, ZERO, ONE};
///
/// fn main() {
///   let zero: b2 = (0 as usize).into();
///   assert_eq!(zero, b2::new([ZERO, ZERO]));
///   
///   let one: b2 = (1 as usize).into();
///   assert_eq!(one, b2::new([ZERO, ONE]));
///   
///   let two: b2 = (2 as usize).into();
///   assert_eq!(two, b2::new([ONE, ZERO]));
///   
///   let three: b2 = (3 as usize).into();
///   assert_eq!(three, b2::new([ONE, ONE]));
/// }
/// ```
///
/// # Panics
///
/// Panics if the argument is larger than what b2 can hold.
///
/// ```should_panic
/// extern crate finite_fields;
/// use finite_fields::binary_static::{b2, ZERO, ONE};
///
/// fn main() {
///   let four = b2::from(4 as usize);
/// }
/// ```
impl From<usize> for b2 {
  fn from(val: usize) -> Self {
    let mut out_val = b2::new([ZERO, ZERO]);
    
    if val > out_val.max() {
      panic!("Can't convert {} to b2, it's too big.", val);
    } else if val == 0 {
      out_val
    } else {
      let mut dec = val.clone();
      let out_val_len = out_val.len();
      if dec % 2 != 0 {
        out_val[out_val_len - 1] = ONE;
      }
      
      for place in 0..out_val.len() {
        if dec > 1 {
          dec = dec / 2;
          out_val[place] = ONE;
        } else {
          break;          
        }      
      }
      
      out_val
    }
  }
}

#[cfg(feature = "nightly")]
/// Fallible conversion by length-checked read through a slice reference.
///
/// # Examples
///
/// ```
/// #![feature(try_from)] extern crate finite_fields;
/// use std::convert::{TryFrom, TryInto};
/// use finite_fields::binary_static::{b2, ZERO, ONE};
///
/// let zero_arr = [ZERO, ZERO];
/// let one_arr = [ZERO, ONE];
///
/// let zero = b2::new([ZERO, ZERO]);
/// let one = b2::new([ZERO, ONE]);
/// assert_eq!(zero, (&zero_arr[0..]).try_into().unwrap());
/// assert_eq!(one, (&one_arr[0..]).try_into().unwrap());
///
/// let bad_arr = [ZERO, ZERO, ZERO];
/// assert!(b2::try_from(&bad_arr[0..]).is_err());
/// ```
impl<'a> TryFrom<&'a [b1]> for b2 {
  type Err = ConversionError;

  fn try_from(val: &[b1]) -> Result<Self, Self::Err> {
    if val.len() != 2 {
      Err(ConversionError::LengthError { arg1: val.len() })
    } else {
      let mut out_val = b2::new([ZERO, ZERO]);
      for place in 0..out_val.len() {
        out_val[place] = val[place];
      }
      Ok(out_val)
    }
  }
}

/// Implements the conversion by converting the bits to `usize` and then
/// summing.
///
/// # Examples
///
/// ```
/// #![feature(try_from)] extern crate finite_fields;
/// use finite_fields::binary_static::{ZERO, ONE, b2};
/// 
/// let one: usize = b2::new([ZERO, ONE]).into();
/// assert_eq!(one, 1);
/// ```
///
/// ```
/// #![feature(try_from)] extern crate finite_fields;
/// use finite_fields::binary_static::{ZERO, ONE, b2};
///
/// let three: usize = b2::new([ONE, ONE]).into();
/// assert_eq!(three, 3);
/// ```
impl From<b2> for usize {
  fn from(val: b2) -> usize {
    val.bits().iter()
              .rev()
              .enumerate()
              .fold(0, |accum, (place_ind, &bit)| {
                usize::from(bit) *
                  (2 as usize).pow((place_ind) as u32) + accum
              })
  }
}

impl<'a> Index<usize> for b2 {
  type Output = b1;

  fn index<'b>(&'b self, index: usize) -> &'b b1 {
    &self.0[index]
  }
}

impl IndexMut<usize> for b2 {
  fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut b1 {
    &mut self.0[index]
  }
}

/// An iterator for b2.
pub struct Iter {
  data: b2,
  index: usize,
  end: usize
}

impl Iterator for Iter {
  type Item = b1;

  // next() is the only required method
  fn next(&mut self) -> Option<b1> {
    self.index += 1;

    // check to see if we've finished counting or not.
    if self.index < 2 && self.index < self.end {
      Some(self.data.0[self.index])
    } else {
      None
    }
  }
}

impl DoubleEndedIterator for Iter {
  fn next_back(&mut self) -> Option<Self::Item> {
    if self.end > 0 {
      self.end -= 1;
      // Compare backwards against the forward index.
      if self.end >= self.index {
        Some(self.data.0[self.end])
      } else {
        None
      }
    } else {
      None
    }
  }
}

impl IntoIterator for b2 {
  type Item = b1;
  type IntoIter = ::std::vec::IntoIter<b1>;

  fn into_iter(self) -> Self::IntoIter {
    self.0.to_vec().into_iter()
  }
}

impl<'a> IntoIterator for &'a b2 {
  type Item = &'a b1;
  type IntoIter = ::std::slice::Iter<'a,b1>;

  fn into_iter(self) -> Self::IntoIter {
    self.0.into_iter()
  }
}

impl FromIterator<b1> for b2 {
  fn from_iter<I: IntoIterator<Item=b1>>(iter: I) -> Self {
    let mut collector = b2::new([ZERO, ZERO]);
    
    for (ind, val) in (0..collector.len()).zip(iter) {
      collector[ind] = val;
    }
    
    collector
  }
}

// impl<'a> Index<usize> for &'a Vec<b2> {
//   type Output = b2;
// 
//   fn index(&'a self, ind: usize) -> &'a b2 {
//     self[ind]
//   }
// }


/// Arithmetic addition with overflow error.
impl Add for b2 {
  type Output = Result<Self, OverflowError>;

  fn add(self, other: b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO; 2]);
    let mut carry = false;
    
    for place in (0..other.0.len()).rev() {
      // Handle the carry flag first if true
      let augend = match carry {
        false => self.0[place].clone(),
        true => match self.0[place].successor() {
          Ok(res) => {
            res.clone()
          },
          Err(_) => {
            ZERO
          }
        }
      };

      // Do the addition
      match augend + other.0[place] {
        Ok(res) => {
          output.0[place] = res;
          carry = false;
        }
        Err(_) => {
          output.0[place] = ZERO;
          carry = true
        }
      }
    }

    if carry {
      Err(OverflowError::Default { arg1: self.to_string(),
                                   arg2: other.to_string() })
    } else {
      Ok(output)
    }
  }
}
impl<'a> Add<b2> for &'a b2 {
  type Output = Result<b2, OverflowError>;

  fn add(self, other: b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO; 2]);
    let mut carry = false;
    
    for place in (0..other.0.len()).rev() {
      // Handle the carry flag first if true
      let augend = match carry {
        false => self.0[place].clone(),
        true => match self.0[place].successor() {
          Ok(res) => {
            res.clone()
          },
          Err(_) => {
            ZERO
          }
        }
      };

      // Do the addition
      match augend + other.0[place] {
        Ok(res) => {
          output.0[place] = res;
          carry = false;
        }
        Err(_) => {
          output.0[place] = ZERO;
          carry = true
        }
      }
    }

    if carry {
      Err(OverflowError::Default { arg1: self.to_string(),
                                   arg2: other.to_string() })
    } else {
      Ok(output)
    }
  }
}
impl<'a> Add<&'a b2> for &'a b2 {
  type Output = Result<b2, OverflowError>;

  fn add(self, other: &b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO; 2]);
    let mut carry = false;
    
    for place in (0..other.0.len()).rev() {
      // Handle the carry flag first if true
      let augend = match carry {
        false => self.0[place].clone(),
        true => match self.0[place].successor() {
          Ok(res) => {
            res.clone()
          },
          Err(_) => {
            ZERO
          }
        }
      };

      // Do the addition
      match augend + other.0[place] {
        Ok(res) => {
          output.0[place] = res;
          carry = false;
        }
        Err(_) => {
          output.0[place] = ZERO;
          carry = true
        }
      }
    }

    if carry {
      Err(OverflowError::Default { arg1: self.to_string(),
                                   arg2: other.to_string() })
    } else {
      Ok(output)
    }
  }
}

/// Wrapping arithmetic subtraction with overflow error.
impl Sub for b2 {
  type Output = Result<b2, OverflowError>;

  fn sub(self, other: b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO; 2]);
    let mut carry = false;
    
    for place in (0..other.0.len()).rev() {
      // Handle the carry flag first if true
      let augend = match carry {
        false => self.0[place],
        true => match self.0[place].predecessor() {
          Ok(res) => {
            res
          },
          Err(_) => {
            ZERO
          }
        }
      };

      // Do the addition
      match augend - other.0[place] {
        Ok(res) => {
          output.0[place] = res;
          carry = false;
        }
        Err(_) => {
          output.0[place] = ONE;
          carry = true
        }
      }
    }

    if carry {
      Err(OverflowError::Default { arg1: self.to_string(),
                                   arg2: other.to_string() })
    } else {
      Ok(output)
    }
  }
}
impl<'a> Sub<b2> for &'a b2 {
  type Output = Result<b2, OverflowError>;

  fn sub(self, other: b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO; 2]);
    let mut carry = false;
    
    for place in (0..other.0.len()).rev() {
      // Handle the carry flag first if true
      let augend = match carry {
        false => self.0[place],
        true => match self.0[place].predecessor() {
          Ok(res) => {
            res
          },
          Err(_) => {
            ZERO
          }
        }
      };

      // Do the addition
      match augend - other.0[place] {
        Ok(res) => {
          output.0[place] = res;
          carry = false;
        }
        Err(_) => {
          output.0[place] = ONE;
          carry = true
        }
      }
    }

    if carry {
      Err(OverflowError::Default { arg1: self.to_string(),
                                   arg2: other.to_string() })
    } else {
      Ok(output)
    }
  }
}
impl<'a> Sub<&'a b2> for &'a b2 {
  type Output = Result<b2, OverflowError>;

  fn sub(self, other: &b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO; 2]);
    let mut carry = false;
    
    for place in (0..other.0.len()).rev() {
      // Handle the carry flag first if true
      let augend = match carry {
        false => self.0[place],
        true => match self.0[place].predecessor() {
          Ok(res) => {
            res
          },
          Err(_) => {
            ZERO
          }
        }
      };

      // Do the addition
      match augend - other.0[place] {
        Ok(res) => {
          output.0[place] = res;
          carry = false;
        }
        Err(_) => {
          output.0[place] = ONE;
          carry = true
        }
      }
    }

    if carry {
      Err(OverflowError::Default { arg1: self.to_string(),
                                   arg2: other.to_string() })
    } else {
      Ok(output)
    }
  }
}

/// This implementation is done "in reverse" of the expected logical order; it
/// uses the `Add` and `Sub` impls instead of the converse.
impl Peano for b2 {
  fn successor(&self) -> Result<b2, OverflowError> {
    self + b2([ZERO, ONE])
  }

  /// Peano arithmetic function.
  fn predecessor(&self) -> Result<b2, OverflowError> {
    self - b2([ZERO, ONE])
  }

  /// Total ordering function.
  fn cmp (&self, other: &Self) -> Ordering {
    if *self == *other { Ordering::Equal }
    else {
      match *self - *other {
        Ok(_) => Ordering::Greater,
        Err(_) => Ordering::Less
      }
    }
  }
}

impl PartialOrd for b2 {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

impl BitAnd for b2 {
  type Output = Self;
  fn bitand(self, other: b2) -> Self {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] & other.0[place]
    }
    output
  }
}
impl<'a> BitAnd<b2> for &'a b2 {
  type Output = b2;
  fn bitand(self, other: b2) -> b2 {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] & other.0[place]
    }
    output
  }
}
impl<'a> BitAnd<&'a b2> for &'a b2 {
  type Output = b2;
  fn bitand(self, other: &b2) -> b2 {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] & other.0[place]
    }
    output
  }
}

impl BitOr for b2 {
  type Output = Self;
  fn bitor(self, other: b2) -> Self {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] | other.0[place]
    }
    output
  }
}
impl<'a> BitOr<b2> for &'a b2 {
  type Output = b2;
  fn bitor(self, other: b2) -> b2 {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] | other.0[place]
    }
    output
  }
}
impl<'a> BitOr<&'a b2> for &'a b2 {
  type Output = b2;
  fn bitor(self, other: &b2) -> b2 {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] | other.0[place]
    }
    output
  }
}

impl BitXor for b2 {
  type Output = Self;
  fn bitxor(self, other: b2) -> Self {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] ^ other.0[place]
    }
    output
  }
}
impl<'a> BitXor<b2> for &'a b2 {
  type Output = b2;
  fn bitxor(self, other: b2) -> b2 {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] ^ other.0[place]
    }
    output
  }
}
impl<'a> BitXor<&'a b2> for &'a b2 {
  type Output = b2;
  fn bitxor(self, other: &b2) -> b2 {
    let mut output = b2([ZERO; 2]);
    for place in (0..other.0.len()).rev() {
      output.0[place] = self.0[place] ^ other.0[place]
    }
    output
  }
}

impl Shr<usize> for b2 {
  type Output = Self;
  fn shr(self, rhs: usize) -> Self {
    let mut dest_arr = [ZERO; 2];
    for dest_ind in 0..dest_arr.len() {
      if dest_ind < rhs {
        dest_arr[dest_ind] = ZERO;
      } else {
        dest_arr[dest_ind] = self.0[dest_ind - rhs];
      }
    }
    b2(dest_arr)
  }
}
impl Shl<usize> for b2 {
  type Output = Self;
  fn shl(self, rhs: usize) -> Self {
    let mut dest_arr = [ZERO; 2];
    for dest_ind in 0..dest_arr.len() {
      if dest_ind < rhs {
        dest_arr[dest_ind] = self.0[dest_ind + rhs];
      } else {
        dest_arr[dest_ind] = ZERO;
      }
    }
    b2(dest_arr)
  }
}

impl Mul for b2 {
  type Output = Result<b2, OverflowError>;
  
  fn mul(self, other: b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO, ZERO]);
    let mut mult_rhs = other.clone();
    let one = b2([ZERO, ONE]);
    let zero = b2([ZERO; 2]);
    
    while mult_rhs > zero {
      if let Ok(sum) = output + self {
        output = sum
      } else {
        // overflow case
        return Err(OverflowError::Default { arg1: self.to_string(),
                                            arg2: other.to_string() })
      }
      mult_rhs = (mult_rhs - one).unwrap();
    }
    
    Ok(output)
  }
}
impl<'a> Mul<b2> for &'a b2 {
  type Output = Result<b2, OverflowError>;
  
  fn mul(self, other: b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO, ZERO]);
    let mut mult_rhs = other.clone();
    let one = b2([ZERO, ONE]);
    let zero = b2([ZERO; 2]);
    
    while mult_rhs > zero {
      if let Ok(sum) = output + *self {
        output = sum
      } else {
        // overflow case
        return Err(OverflowError::Default { arg1: self.to_string(),
                                            arg2: other.to_string() })
      }
      mult_rhs = (mult_rhs - one).unwrap();
    }
    
    Ok(output)
  }
}
impl<'a> Mul<&'a b2> for &'a b2 {
  type Output = Result<b2, OverflowError>;
  
  fn mul(self, other: &'a b2) -> Result<b2, OverflowError> {
    let mut output = b2([ZERO, ZERO]);
    let mut mult_rhs = other.clone();
    let one = b2([ZERO, ONE]);
    let zero = b2([ZERO; 2]);
    
    while mult_rhs > zero {
      if let Ok(sum) = output + *self {
        output = sum
      } else {
        // overflow case
        return Err(OverflowError::Default { arg1: self.to_string(),
                                            arg2: other.to_string() })
      }
      mult_rhs = (mult_rhs - one).unwrap();
    }
    
    Ok(output)
  }
}
/// Implementation of division as repeated subtraction.
impl Div<b2> for b2 {
  type Output = Result<b2, DivisionError>;
  
  fn div(self, other: b2) -> Result<b2, DivisionError> {
    let one = b2([ZERO, ONE]);
    let zero = b2([ZERO; 2]);

    if other == zero {
      return Err(DivisionError::DivideByZeroError { arg1: self.to_string() });
    } else if self < other {
      // division is guaranteed to overflow in this case
      return Err(DivisionError::OverflowError { arg1: self.to_string(),
                                                arg2: other.to_string() })
    }
    
    let mut div_lhs = other.clone();
    let mut sub_count = zero;
    
    while div_lhs > other {
      if let Ok(difference) = div_lhs - other {
        div_lhs = difference;
        if let Ok(sum) = sub_count + one {
          sub_count = sum;
        } else {
          return Err(DivisionError::OverflowError { arg1: self.to_string(),
                                                    arg2: other.to_string() })
        }
      } else {
        break;
      }
    }

    Ok(sub_count)
  }
}
impl<'a> Div<b2> for &'a b2 {
  type Output = Result<b2, DivisionError>;
  
  fn div(self, other: b2) -> Result<b2, DivisionError> {
    let one = b2([ZERO, ONE]);
    let zero = b2([ZERO; 2]);

    if other == zero {
      return Err(DivisionError::DivideByZeroError { arg1: self.to_string() })
    } else if self < &other {
      // division is guaranteed to overflow in this case
      return Err(DivisionError::OverflowError { arg1: self.to_string(),
                                                arg2: other.to_string() })
    }
    
    let mut div_lhs = other.clone();
    let mut sub_count = zero;
    
    while div_lhs > other {
      if let Ok(difference) = div_lhs - other {
        div_lhs = difference;
        if let Ok(sum) = sub_count + one {
          sub_count = sum;
        } else {
          return Err(DivisionError::OverflowError { arg1: self.to_string(),
                                                    arg2: other.to_string() })
        }
      } else {
        break;
      }
    }

    Ok(sub_count)
  }
}
impl<'a> Div<&'a b2> for &'a b2 {
  type Output = Result<b2, DivisionError>;
  
  fn div(self, other: &b2) -> Result<b2, DivisionError> {
    let one = b2([ZERO, ONE]);
    let zero = b2([ZERO; 2]);

    if *other == zero {
      return Err(DivisionError::DivideByZeroError { arg1: self.to_string() })
    } else if self < other {
      // division is guaranteed to overflow in this case
      return Err(DivisionError::OverflowError { arg1: self.to_string(),
                                                arg2: other.to_string() })
    }
    
    let mut div_lhs = other.clone();
    let mut sub_count = zero;
    
    while div_lhs > *other {
      if let Ok(difference) = div_lhs - *other {
        div_lhs = difference;
        if let Ok(sum) = sub_count + one {
          sub_count = sum;
        } else {
          return Err(DivisionError::OverflowError { arg1: self.to_string(),
                                                    arg2: other.to_string() })
        }
      } else {
        break;
      }
    }

    Ok(sub_count)
  }
}
