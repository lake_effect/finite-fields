error_def! OverflowError {
  Default { arg1: String, arg2: String }
    => "Arithmetic overflow." ("Arguments resulting in overflow  were {} and {}", arg1, arg2)
}

error_def! DivisionError {
  DivideByZeroError { arg1: String }
    => "Divide by zero error." ("Attempted to divide {} by zero.", arg1),
  OverflowError { arg1: String, arg2: String }
    => "Arithmetic overflow." ("Arguments resulting in overflow  were {} and {}", arg1, arg2)
}

error_def! ConversionError {
  OverflowError { arg1: String }
    => "Couldn't hold the target value in this type." ("Tried to convert {}.", arg1),
  PrecisionError { arg1: String, tgt_width: String }
    => "Precision lost in conversion to finite field type." ("Tried to convert {} into a width-{} type.", arg1, tgt_width),
  LengthError { arg1: usize }
    => "Array passed in was the wrong length." ("Length was {}", arg1)
}
