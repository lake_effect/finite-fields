//! *Note: all types are little endian.*

#![allow(non_camel_case_types)]
#![feature(plugin)]
#![plugin(error_def)]

#![cfg_attr(feature = "nightly", feature(try_from))]

#[macro_use] extern crate newtype_derive;

/// A couple of binary types defined without macros for illustration purposes.
pub mod binary_static;
/// Macros for generating binary finite field types.
pub mod binary_fields;
/// Macros for generating n-ary finite field types.
pub mod fields;
/// Custom error for finite field arithmetic arrors.
pub mod error;
